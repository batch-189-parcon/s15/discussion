//for outputting data or text into the browser console
console.log('Hello World!')

//[SYNTAX AND STATEMENTS]
//Syntax is the code that makes up a statement
// console
// log()

//Statements are made up of syntax that will be run by the browser
//console.log()

//[COMMENTS]
// this is a single line comment for short description

/*
This is a multi-line comment for longer descritions
and multi-line paragraphs
*/

//[VARIABLES]
//let variables are variables that CAN be re-assigned
let firstName = 'Tristan';
console.log(firstName);

let lastName = 'Marcial';
console.log(lastName);

//Re-assigning a value to let a variable shows no errors
firstName = 'Lylle';
console.log(firstName)

//const variables are variables that CANNOT be re-assigned
const colorOfTheSun = 'yellow';
console.log('the color of the sun is '+ colorOfTheSun)

//Error happens when you try to re-assign value of const variable
// colorOfTheSun = 'red'
// console.log(colorOfTheSun)

//When declaring a variable, you use a keyword like 'let'
let variableName = 'value'

//When re-assigning a value to a variable, you just need the variable name
variableName = 'New Value'

//DATA TYPES
//1. String - Denoted by single OR double quotation marks
let personName = 'Earl Diaz'

//2. Number - No quotation marks and numerical value
let personAge = 15

//3. Boolean - Only 'true' or 'false'
let hasGirlfriend = false

//4. Array - Denoted by brackets and can contain multiple values inside
let hobbies = ['Cycling', 'Reading', 'Coding']

//5. Object - Denoted by curly braces, and has value name/label for each value
let person = {
	personName: 'Earl Diaz',
	personAge: 15,
	hasGirlfriend: false,
	hobbies: ['Cycling', 'Reading', 'Coding']

}

//6. Null - A placeholder for future variable re-assignments
let wallet = null

//console.log each of the variable
console.log(personName)
console.log(personAge)
console.log(hasGirlfriend)
console.log(hobbies)
console.log(person)
console.log(wallet)

//to display single value of an object:
console.log(person.personAge)

//to display single value of the array:
console.log(hobbies[1])
